﻿using System;
using Test_Assignment.Classes.Models;
using Test_Assignment.Classes.Views;

namespace Test_Assignment.Classes.Commands.Abstract
{
    public abstract class UtilityCommand : Command
    {
        protected UtilityCommand(SceneEditorView sceneEditorView) : base(sceneEditorView)
        {
        }

        protected UtilityCommand(Command command) : base(command)
        {
        }
    }
}