﻿using System;
using Test_Assignment.Classes.Models;
using Test_Assignment.Classes.Views;

namespace Test_Assignment.Classes.Commands.Abstract
{
    public abstract class ShapeCommand : Command
    {
        protected ShapeCommand(SceneEditorView sceneEditorView) : base(sceneEditorView)
        {
        }

        protected ShapeCommand(Command command) : base(command)
        {
        }
    }
}