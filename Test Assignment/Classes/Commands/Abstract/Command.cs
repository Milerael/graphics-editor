﻿using System;
using Test_Assignment.Classes.Models;
using Test_Assignment.Classes.Views;
using Test_Assignment.Interfaces;

namespace Test_Assignment.Classes.Commands.Abstract
{
    public abstract class Command
    {
        protected SceneEditorView SceneEditorView;
        protected Scene SceneBackup = new Scene();
        
        public Command(SceneEditorView sceneEditorView)
        {
            InitializeCommand(sceneEditorView, new Scene());
        }

        public Command(Command command)
        {
            InitializeCommand(command.SceneEditorView, command.SceneBackup);
        }
        
        protected void SaveBackup()
        {
            SceneBackup = SceneEditorView.CurrentScene.GetSceneBackup();
        }

        public void Undo()
        {
            SceneEditorView.CurrentScene.LoadBackup(SceneBackup);
        }

        protected void InitializeCommand(SceneEditorView sceneEditorView, Scene sceneBackup)
        {
            SceneEditorView = sceneEditorView;
            SceneBackup = sceneBackup.GetSceneBackup();
        }
        
        public abstract bool Execute(ICommandArguments commandArguments);
        public abstract Command GetCommandCopy();
    }
}