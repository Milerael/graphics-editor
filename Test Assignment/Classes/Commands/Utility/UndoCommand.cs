﻿using System;
using Test_Assignment.Classes.Commands.Abstract;
using Test_Assignment.Classes.Views;
using Test_Assignment.Interfaces;
using Test_Assignment.Structs.CommandArguments.Utility;

namespace Test_Assignment.Classes.Commands.Utility
{
    public class UndoCommand : UtilityCommand
    {
        public UndoCommand(SceneEditorView sceneEditorView) : base(sceneEditorView)
        {
        }

        public UndoCommand(Command command) : base(command)
        {
        }

        public override bool Execute(ICommandArguments commandArguments)
        {
            if (commandArguments is UndoCommandArguments undoCommandArguments)
            {
                SceneEditorView.UndoCommand();
                return false;
            }
            
            Console.WriteLine("Invalid Command Arguments!");
            return false;
        }

        public override Command GetCommandCopy()
        {
            var commandCopy = new UndoCommand(this);
            return commandCopy;
        }
    }
}