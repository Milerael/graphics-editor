﻿using System;
using System.IO;
using System.Xml.Serialization;
using Test_Assignment.Classes.Commands.Abstract;
using Test_Assignment.Classes.Models;
using Test_Assignment.Classes.Views;
using Test_Assignment.Interfaces;
using Test_Assignment.Structs.CommandArguments.Utility;

namespace Test_Assignment.Classes.Commands.Utility
{
    public class SaveSceneCommand : UtilityCommand
    {
        public SaveSceneCommand(SceneEditorView sceneEditorView) : base(sceneEditorView)
        {
        }

        public SaveSceneCommand(Command command) : base(command)
        {
        }

        public override bool Execute(ICommandArguments commandArguments)
        {
            if (commandArguments is SaveSceneCommandArguments saveSceneCommandArguments)
            {
                var serializer = new XmlSerializer(typeof(Scene));
                var writer = new StreamWriter(saveSceneCommandArguments.Path);
                serializer.Serialize(writer, SceneEditorView.CurrentScene);
                writer.Close();
                return false;
            }
            
            Console.WriteLine("Invalid Command Arguments!");
            return false;
        }

        public override Command GetCommandCopy()
        {
            var commandCopy = new SaveSceneCommand(this);
            return commandCopy;
        }
    }
}