﻿using System;
using Test_Assignment.Classes.Commands.Abstract;
using Test_Assignment.Classes.Models;
using Test_Assignment.Classes.Views;
using Test_Assignment.Interfaces;
using Test_Assignment.Structs.CommandArguments.Utility;

namespace Test_Assignment.Classes.Commands.Utility
{
    public class CreateSceneCommand : UtilityCommand
    {
        public CreateSceneCommand(SceneEditorView sceneEditorView) : base(sceneEditorView)
        {
        }

        public CreateSceneCommand(Command command) : base(command)
        {
        }

        public override bool Execute(ICommandArguments commandArguments)
        {
            if (commandArguments is CreateSceneCommandArguments createSceneCommandArguments)
            {
                SceneEditorView.CurrentScene.LoadBackup(new Scene());
                SceneEditorView.CommandHistory.Clear();
                return false;
            }
            
            Console.WriteLine("Invalid Command Arguments!");
            return false;
        }

        public override Command GetCommandCopy()
        {
            var commandCopy = new CreateSceneCommand(this);
            return commandCopy;
        }
    }
}