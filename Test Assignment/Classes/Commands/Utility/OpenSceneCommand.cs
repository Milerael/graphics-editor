﻿using System;
using System.IO;
using System.Xml.Serialization;
using Test_Assignment.Classes.Commands.Abstract;
using Test_Assignment.Classes.Models;
using Test_Assignment.Classes.Views;
using Test_Assignment.Interfaces;
using Test_Assignment.Structs.CommandArguments.Utility;

namespace Test_Assignment.Classes.Commands.Utility
{
    public class OpenSceneCommand : UtilityCommand
    {
        public OpenSceneCommand(SceneEditorView sceneEditorView) : base(sceneEditorView)
        {
        }

        public OpenSceneCommand(Command command) : base(command)
        {
        }

        public override bool Execute(ICommandArguments commandArguments)
        {
            if (commandArguments is OpenSceneCommandArguments openSceneCommandArguments)
            {
                var serializer = new XmlSerializer(typeof(Scene));
                var fileStream = new FileStream(openSceneCommandArguments.Path, FileMode.Open);
                var scene = (Scene) serializer.Deserialize(fileStream);
                SceneEditorView.CurrentScene.LoadBackup(scene);
                fileStream.Close();
                SceneEditorView.CommandHistory.Clear();
                return false;
            }
            
            Console.WriteLine("Invalid Command Arguments!");
            return false;
        }

        public override Command GetCommandCopy()
        {
            var commandCopy = new OpenSceneCommand(this);
            return commandCopy;
        }
    }
}