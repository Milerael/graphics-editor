﻿using System;
using Test_Assignment.Classes.Commands.Abstract;
using Test_Assignment.Classes.Views;
using Test_Assignment.Classes.Views.Shapes;
using Test_Assignment.Interfaces;
using Test_Assignment.Structs.CommandArguments.Shape;

namespace Test_Assignment.Classes.Commands.Shape
{
    public class EditRectangleCommand : ShapeCommand
    {
        public EditRectangleCommand(SceneEditorView sceneEditorView) : base(sceneEditorView)
        {
        }

        public EditRectangleCommand(Command command) : base(command)
        {
        }
        
        public override bool Execute(ICommandArguments commandArguments)
        {
            SaveBackup();
            
            if (commandArguments is EditRectangleCommandArguments editRectangleCommandArguments)
            {
                if (editRectangleCommandArguments.ShapeIndex < 0 || editRectangleCommandArguments.ShapeIndex >= SceneEditorView.CurrentScene.Shapes.Count) return false;
                var rectangle = SceneEditorView.CurrentScene.Shapes[editRectangleCommandArguments.ShapeIndex] as RectangleShapeView;
                rectangle?.EditShape(editRectangleCommandArguments);
                return true;
            }

            Console.WriteLine("Invalid Command Arguments!");
            return false;
        }

        public override Command GetCommandCopy()
        {
            var commandCopy = new EditRectangleCommand(this);
            return commandCopy;
        }
    }
}