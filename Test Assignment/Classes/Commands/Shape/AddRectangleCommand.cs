﻿using System;
using Test_Assignment.Classes.Commands.Abstract;
using Test_Assignment.Classes.Views;
using Test_Assignment.Classes.Views.Shapes;
using Test_Assignment.Interfaces;
using Test_Assignment.Structs.CommandArguments.Shape;

namespace Test_Assignment.Classes.Commands.Shape
{
    public class AddRectangleCommand : ShapeCommand
    {
        public AddRectangleCommand(SceneEditorView sceneEditorView) : base(sceneEditorView)
        {
        }

        public AddRectangleCommand(Command command) : base(command)
        {
        }
        
        public override bool Execute(ICommandArguments commandArguments)
        {
            SaveBackup();
            
            if (commandArguments is AddRectangleCommandArguments addRectangleCommandArguments)
            {
                SceneEditorView.CurrentScene.Shapes.Add(new RectangleShapeView(addRectangleCommandArguments));
                return true;
            }

            Console.WriteLine("Invalid Command Arguments!");
            return false;
        }

        public override Command GetCommandCopy()
        {
            var commandCopy = new AddRectangleCommand(this);
            return commandCopy;
        }
    }
}