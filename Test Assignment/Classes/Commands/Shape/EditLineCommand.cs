﻿using System;
using Test_Assignment.Classes.Commands.Abstract;
using Test_Assignment.Classes.Views;
using Test_Assignment.Classes.Views.Shapes;
using Test_Assignment.Interfaces;
using Test_Assignment.Structs.CommandArguments.Shape;

namespace Test_Assignment.Classes.Commands.Shape
{
    public class EditLineCommand : ShapeCommand
    {
        public EditLineCommand(SceneEditorView sceneEditorView) : base(sceneEditorView)
        {
        }

        public EditLineCommand(Command command) : base(command)
        {
        }
        
        public override bool Execute(ICommandArguments commandArguments)
        {
            SaveBackup();
            
            if (commandArguments is EditLineCommandArguments editLineCommandArguments)
            {
                if (editLineCommandArguments.ShapeIndex < 0 || editLineCommandArguments.ShapeIndex >= SceneEditorView.CurrentScene.Shapes.Count) return false;
                var line = SceneEditorView.CurrentScene.Shapes[editLineCommandArguments.ShapeIndex] as LineShapeView;
                line?.EditShape(editLineCommandArguments);
                return true;
            }

            Console.WriteLine("Invalid Command Arguments!");
            return false;
        }

        public override Command GetCommandCopy()
        {
            var commandCopy = new EditLineCommand(this);
            return commandCopy;
        }
    }
}