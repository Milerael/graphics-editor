﻿using System;
using Test_Assignment.Classes.Commands.Abstract;
using Test_Assignment.Classes.Views;
using Test_Assignment.Interfaces;
using Test_Assignment.Structs.CommandArguments.Shape;

namespace Test_Assignment.Classes.Commands.Shape
{
    public class RemoveShapesCommand : ShapeCommand
    {
        public RemoveShapesCommand(SceneEditorView sceneEditorView) : base(sceneEditorView)
        {
        }

        public RemoveShapesCommand(Command command) : base(command)
        {
        }
        
        public override bool Execute(ICommandArguments commandArguments)
        {
            SaveBackup();
            
            if (commandArguments is RemoveShapesCommandArguments removeShapesCommandArguments)
            {
                foreach (var shapeIndex in removeShapesCommandArguments.ShapesToRemove)
                {
                    if (shapeIndex < 0 || shapeIndex >= SceneEditorView.CurrentScene.Shapes.Count) continue;
                    
                    SceneEditorView.CurrentScene.Shapes.RemoveAt(shapeIndex);
                }

                return true;
            }

            Console.WriteLine("Invalid Command Arguments!");
            return false;
        }

        public override Command GetCommandCopy()
        {
            var commandCopy = new RemoveShapesCommand(this);
            return commandCopy;
        }
    }
}