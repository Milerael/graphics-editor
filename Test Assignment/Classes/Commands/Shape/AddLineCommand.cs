﻿using System;
using Test_Assignment.Classes.Commands.Abstract;
using Test_Assignment.Classes.Views;
using Test_Assignment.Classes.Views.Shapes;
using Test_Assignment.Interfaces;
using Test_Assignment.Structs.CommandArguments.Shape;

namespace Test_Assignment.Classes.Commands.Shape
{
    public class AddLineCommand : ShapeCommand
    {
        public AddLineCommand(SceneEditorView sceneEditorView) : base(sceneEditorView)
        {
        }

        public AddLineCommand(Command command) : base(command)
        {
        }
        
        public override bool Execute(ICommandArguments commandArguments)
        {
            SaveBackup();
            
            if (commandArguments is AddLineCommandArguments addLineCommandArguments)
            {
                SceneEditorView.CurrentScene.Shapes.Add(new LineShapeView(addLineCommandArguments));
                return true;
            }

            Console.WriteLine("Invalid Command Arguments!");
            return false;
        }

        public override Command GetCommandCopy()
        {
            var commandCopy = new AddLineCommand(this);
            return commandCopy;
        }
    }
}