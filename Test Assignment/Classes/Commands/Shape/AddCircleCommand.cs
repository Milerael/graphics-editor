﻿using System;
using Test_Assignment.Classes.Commands.Abstract;
using Test_Assignment.Classes.Models;
using Test_Assignment.Classes.Views;
using Test_Assignment.Classes.Views.Shapes;
using Test_Assignment.Interfaces;
using Test_Assignment.Structs.CommandArguments.Shape;

namespace Test_Assignment.Classes.Commands.Shape
{
    public class AddCircleCommand : ShapeCommand
    {
        public AddCircleCommand(SceneEditorView sceneEditorView) : base(sceneEditorView)
        {
        }

        public AddCircleCommand(Command command) : base(command)
        {
        }
        
        public override bool Execute(ICommandArguments commandArguments)
        {
            SaveBackup();
            
            if (commandArguments is AddCircleCommandArguments addCircleCommandArguments)
            {
                SceneEditorView.CurrentScene.Shapes.Add(new CircleShapeView(addCircleCommandArguments));
                return true;
            }

            Console.WriteLine("Invalid Command Arguments!");
            return false;
        }

        public override Command GetCommandCopy()
        {
            var commandCopy = new AddCircleCommand(this);
            return commandCopy;
        }
    }
}