﻿using System;
using Test_Assignment.Classes.Commands.Abstract;
using Test_Assignment.Classes.Views;
using Test_Assignment.Classes.Views.Shapes;
using Test_Assignment.Interfaces;
using Test_Assignment.Structs.CommandArguments.Shape;

namespace Test_Assignment.Classes.Commands.Shape
{
    public class EditCircleCommand : ShapeCommand
    {
        public EditCircleCommand(SceneEditorView sceneEditorView) : base(sceneEditorView)
        {
        }

        public EditCircleCommand(Command command) : base(command)
        {
        }
        
        public override bool Execute(ICommandArguments commandArguments)
        {
            SaveBackup();
            
            if (commandArguments is EditCircleCommandArguments editCircleCommandArguments)
            {
                if (editCircleCommandArguments.ShapeIndex < 0 || editCircleCommandArguments.ShapeIndex >= SceneEditorView.CurrentScene.Shapes.Count) return false;
                var circle = SceneEditorView.CurrentScene.Shapes[editCircleCommandArguments.ShapeIndex] as CircleShapeView;
                circle?.EditShape(editCircleCommandArguments);
                return true;
            }

            Console.WriteLine("Invalid Command Arguments!");
            return false;
        }

        public override Command GetCommandCopy()
        {
            var commandCopy = new EditCircleCommand(this);
            return commandCopy;
        }
    }
}