﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Test_Assignment.Classes.Views;
using Test_Assignment.Classes.Views.Abstract;

namespace Test_Assignment.Classes.Models
{
    [Serializable]
    public class Scene
    {
        [XmlArray]
        public List<ShapeView> Shapes = new List<ShapeView>();

        public Scene()
        {
            
        }
        
        private Scene(Scene scene)
        {
            LoadBackup(scene);
        }
        
        public void LoadBackup(Scene scene)
        {
            Shapes = new List<ShapeView>(scene.Shapes);
        }

        public Scene GetSceneBackup()
        {
            var backup = new Scene(this);
            return backup;
        }
        
        public void Draw()
        {
            Console.WriteLine("Current Scene:");

            for (var i = 0; i < Shapes.Count; i++)
            {
                var shape = Shapes[i];
                Console.Write($"{i}: ");
                shape.Draw();
            }
        }
    }
}