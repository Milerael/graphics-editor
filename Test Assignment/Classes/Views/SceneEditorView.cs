﻿using System;
using System.Collections.Generic;
using System.Linq;
using Test_Assignment.Classes.Commands.Abstract;
using Test_Assignment.Classes.Commands.Shape;
using Test_Assignment.Classes.Commands.Utility;
using Test_Assignment.Classes.Models;
using Test_Assignment.Classes.Views.Shapes;
using Test_Assignment.Interfaces;
using Test_Assignment.Structs.CommandArguments.Shape;
using Test_Assignment.Structs.CommandArguments.Utility;
using Test_Assignment.Structs.Utility;

namespace Test_Assignment.Classes.Views
{
    public class SceneEditorView
    {
        private const int stackSize = 10;

        public Scene CurrentScene;

        public LinkedList<Command> CommandHistory = new LinkedList<Command>();
        private Random random;

        public SceneEditorView()
        {
            Console.WriteLine("Starting Editor!");
            CurrentScene = new Scene();

            random = new Random();

            //Commands
            var addCircleCommand = new AddCircleCommand(this);
            var addRectangleCommand = new AddRectangleCommand(this);
            var addLineCommand = new AddLineCommand(this);
            var editCircleCommand = new EditCircleCommand(this);
            var editRectangleCommand = new EditRectangleCommand(this);
            var editLineCommand = new EditLineCommand(this);
            var removeShapesCommand = new RemoveShapesCommand(this);

            var openSceneCommand = new OpenSceneCommand(this);
            var saveSceneCommand = new SaveSceneCommand(this);
            var createSceneCommand = new CreateSceneCommand(this);
            var undoCommand = new UndoCommand(this);

            //Actions
            var addCircleAction =
                new Action<ICommandArguments>((arguments) => executeCommand(addCircleCommand, arguments));
            var addRectangleAction =
                new Action<ICommandArguments>((arguments) => executeCommand(addRectangleCommand, arguments));
            var addLineAction =
                new Action<ICommandArguments>((arguments) => executeCommand(addLineCommand, arguments));
            var editCircleAction =
                new Action<ICommandArguments>((arguments) => executeCommand(editCircleCommand, arguments));
            var editRectangleAction =
                new Action<ICommandArguments>((arguments) => executeCommand(editRectangleCommand, arguments));
            var editLineAction =
                new Action<ICommandArguments>((arguments) => executeCommand(editLineCommand, arguments));
            var removeShapesAction =
                new Action<ICommandArguments>((arguments) => executeCommand(removeShapesCommand, arguments));

            var openSceneAction =
                new Action<ICommandArguments>((arguments) => executeCommand(openSceneCommand, arguments));
            var saveSceneAction =
                new Action<ICommandArguments>((arguments) => executeCommand(saveSceneCommand, arguments));
            var createSceneAction =
                new Action<ICommandArguments>((arguments) => executeCommand(createSceneCommand, arguments));
            var undoAction =
                new Action<ICommandArguments>((arguments) => executeCommand(undoCommand, arguments));

            //Binding Action To UIButtons

            /*  
            var openSceneButton = new UIButton(openSceneAction);
            */

            //Debug Input
            while (true)
            {
                Console.WriteLine();
                Console.WriteLine("Input numbers 1-11");
                Console.WriteLine("1 - Add Random Circle");
                Console.WriteLine("2 - Add Random Rectangle");
                Console.WriteLine("3 - Add Random Line");
                Console.WriteLine("4 - Edit Random Line");
                Console.WriteLine("5 - Edit Random Rectangle");
                Console.WriteLine("6 - Edit Random Circle");
                Console.WriteLine("7 - Remove Random Shape");
                Console.WriteLine("8 - Save Scene");
                Console.WriteLine("9 - Open Scene");
                Console.WriteLine("10 - Create Scene");
                Console.WriteLine("11 - Undo");
                Console.WriteLine();
                
                var lineIndices = CurrentScene.Shapes
                    .Select((a, i) => new {Value = a, Index = i})
                    .Where(shape => shape.Value is LineShapeView)
                    .Select(shape => shape.Index).ToArray();
                
                
                var circleIndices = CurrentScene.Shapes
                    .Select((a, i) => new {Value = a, Index = i})
                    .Where(shape => shape.Value is CircleShapeView)
                    .Select(shape => shape.Index).ToArray();
                
                
                var rectanglesIndices = CurrentScene.Shapes
                    .Select((a, i) => new {Value = a, Index = i})
                    .Where(shape => shape.Value is RectangleShapeView)
                    .Select(shape => shape.Index).ToArray();
                
                
                
                var input = Convert.ToInt32(Console.ReadLine());

                switch (input)
                {
                    case 1:
                        addCircleAction.Invoke(new AddCircleCommandArguments(
                            new Vector2(random.Next(500), random.Next(500)),
                            random.Next(50),
                            new Color(random.Next(256), random.Next(256), random.Next(256)),
                            new OutlineProperties(random.Next(10))));
                        break;
                    case 2:
                        addRectangleAction.Invoke(new AddRectangleCommandArguments(
                            new Vector2(random.Next(500), random.Next(500)),
                            new Vector2(random.Next(50), random.Next(50)),
                            new Color(random.Next(256), random.Next(256), random.Next(256)),
                            new OutlineProperties(random.Next(10))));
                        break;
                    case 3:
                        addLineAction.Invoke(new AddLineCommandArguments(
                            new Vector2(random.Next(500), random.Next(500)),
                            new Vector2(random.Next(500), random.Next(500)),
                            new Color(random.Next(256), random.Next(256), random.Next(256)),
                            new OutlineProperties(random.Next(10))));
                        break;
                    case 4:
                        editLineAction.Invoke(new EditLineCommandArguments(
                            lineIndices[random.Next(lineIndices.Length)],
                            new Vector2(random.Next(500), random.Next(500)),
                            new Vector2(random.Next(500), random.Next(500)),
                            new Color(random.Next(256), random.Next(256), random.Next(256)),
                            new OutlineProperties(random.Next(10))));
                        break;
                    case 5:
                        editRectangleAction.Invoke(new EditRectangleCommandArguments(
                            rectanglesIndices[random.Next(rectanglesIndices.Length)],
                            new Vector2(random.Next(500), random.Next(500)),
                            new Vector2(random.Next(50), random.Next(50)),
                            new Color(random.Next(256), random.Next(256), random.Next(256)),
                            new OutlineProperties(random.Next(10))));
                        break;
                    case 6:
                        editCircleAction.Invoke(new EditCircleCommandArguments(
                            circleIndices[random.Next(circleIndices.Length)],
                            new Vector2(random.Next(500), random.Next(500)),
                            random.Next(50),
                            new Color(random.Next(256), random.Next(256), random.Next(256)),
                            new OutlineProperties(random.Next(10))));
                        break;

                    case 7:
                        removeShapesAction.Invoke(
                            new RemoveShapesCommandArguments(random.Next(CurrentScene.Shapes.Count)));
                        break;

                    case 8:
                        saveSceneAction.Invoke(new SaveSceneCommandArguments("Scene1.txt"));
                        break;

                    case 9:
                        openSceneAction.Invoke(new OpenSceneCommandArguments("Scene1.txt"));
                        break;

                    case 10:
                        createSceneAction.Invoke(new CreateSceneCommandArguments());
                        break;

                    case 11:
                        undoAction.Invoke(new UndoCommandArguments());
                        break;

                    default:
                        break;
                }
            }
        }

        private void executeCommand(Command command, ICommandArguments commandArguments)
        {
            if (command.Execute(commandArguments))
            {
                CommandHistory.AddFirst(command.GetCommandCopy());
                if (CommandHistory.Count > stackSize)
                {
                    CommandHistory.RemoveLast();
                }
            }

            CurrentScene.Draw();
        }

        public void UndoCommand()
        {
            if (CommandHistory.Count <= 0) return;

            var command = CommandHistory.First.Value;
            command.Undo();
            CommandHistory.RemoveFirst();

            CurrentScene.Draw();
        }
    }
}