﻿using System;
using System.Xml.Serialization;
using Test_Assignment.Classes.Views.Abstract;
using Test_Assignment.Structs.CommandArguments.Shape;

namespace Test_Assignment.Classes.Views.Shapes
{
    [Serializable]
    public sealed class CircleShapeView : ShapeView
    {
        public float Radius;

        public CircleShapeView()
        {
            
        }

        public CircleShapeView(AddCircleCommandArguments addCircleCommandArguments)
        {
            Position = addCircleCommandArguments.Position;
            Radius = addCircleCommandArguments.Radius;
            Color = addCircleCommandArguments.Color;
            OutlineProperties = addCircleCommandArguments.OutlineProperties;
        }

        public void EditShape(EditCircleCommandArguments editCircleCommandArguments)
        {
            Position = editCircleCommandArguments.Position;
            Radius = editCircleCommandArguments.Radius;
            Color = editCircleCommandArguments.Color;
            OutlineProperties = editCircleCommandArguments.OutlineProperties;
        }

        public override void Draw()
        {
            Console.WriteLine($"Circle : Position - {Position.Formatted}, Radius - {Radius}, Color - {Color.Formatted}, OutlineProperties - {OutlineProperties.Formatted}");
        }
    }
}