﻿using System;
using System.Xml.Serialization;
using Test_Assignment.Classes.Views.Abstract;
using Test_Assignment.Structs.CommandArguments.Shape;
using Test_Assignment.Structs.Utility;

namespace Test_Assignment.Classes.Views.Shapes
{
    [Serializable]
    public sealed class RectangleShapeView : ShapeView
    {
        public Vector2 Size = new Vector2();

        public RectangleShapeView()
        {
            
        }

        public RectangleShapeView(AddRectangleCommandArguments addRectangleCommandArguments)
        {
            Position = addRectangleCommandArguments.Position;
            Size = addRectangleCommandArguments.Size;
            Color = addRectangleCommandArguments.Color;
            OutlineProperties = addRectangleCommandArguments.OutlineProperties;
        }
        
        public void EditShape(EditRectangleCommandArguments editRectangleCommandArguments)
        {
            Position = editRectangleCommandArguments.Position;
            Size = editRectangleCommandArguments.Size;
            Color = editRectangleCommandArguments.Color;
            OutlineProperties = editRectangleCommandArguments.OutlineProperties;
        }

        public override void Draw()
        {
            Console.WriteLine($"Rectangle : Position - {Position.Formatted}, Size - {Size.Formatted}, Color - {Color.Formatted}, OutlineProperties - {OutlineProperties.Formatted}");
        }
    }
}