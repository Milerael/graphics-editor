﻿using System;
using System.Xml.Serialization;
using Test_Assignment.Classes.Views.Abstract;
using Test_Assignment.Structs.CommandArguments.Shape;
using Test_Assignment.Structs.Utility;

namespace Test_Assignment.Classes.Views.Shapes
{
    [Serializable]
    public sealed class LineShapeView : ShapeView
    {
        public Vector2 StartPosition;
        public Vector2 EndPosition;

        public LineShapeView()
        {
            
        }

        public LineShapeView(AddLineCommandArguments addLineCommandArguments)
        {
            Position = addLineCommandArguments.StartPosition;
            StartPosition = addLineCommandArguments.StartPosition;
            EndPosition = addLineCommandArguments.EndPosition;
            Color = addLineCommandArguments.Color;
            OutlineProperties = addLineCommandArguments.OutlineProperties;
        }
        
        public void EditShape(EditLineCommandArguments editLineCommandArguments)
        {
            Position = editLineCommandArguments.StartPosition;
            StartPosition = editLineCommandArguments.StartPosition;
            EndPosition = editLineCommandArguments.EndPosition;
            Color = editLineCommandArguments.Color;
            OutlineProperties = editLineCommandArguments.OutlineProperties;
        }

        public override void Draw()
        {
            Console.WriteLine($"Line : Start Position - {StartPosition.Formatted}, EndPosition - {EndPosition.Formatted}, Color - {Color.Formatted}, OutlineProperties - {OutlineProperties.Formatted}");
        }
    }
}