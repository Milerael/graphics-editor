﻿using System;
using System.Xml.Serialization;
using Test_Assignment.Classes.Views.Shapes;
using Test_Assignment.Structs.Utility;

namespace Test_Assignment.Classes.Views.Abstract
{
    [XmlInclude(typeof(CircleShapeView))]
    [XmlInclude(typeof(RectangleShapeView))]
    [XmlInclude(typeof(LineShapeView))]
    [Serializable]
    public abstract class ShapeView
    {
        public Vector2 Position = new Vector2();
        public Color Color = new Color();
        public OutlineProperties OutlineProperties = new OutlineProperties();
        
        public abstract void Draw();
    }
}