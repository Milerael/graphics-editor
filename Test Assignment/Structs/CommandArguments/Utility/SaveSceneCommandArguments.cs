﻿using Test_Assignment.Interfaces;

namespace Test_Assignment.Structs.CommandArguments.Utility
{
    public struct SaveSceneCommandArguments : IUtilityCommandArguments
    {
        public string Path;

        public SaveSceneCommandArguments(string path)
        {
            Path = path;
        }
    }
}