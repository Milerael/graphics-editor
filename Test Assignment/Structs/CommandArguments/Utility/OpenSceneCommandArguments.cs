﻿using Test_Assignment.Interfaces;

namespace Test_Assignment.Structs.CommandArguments.Utility
{
    public struct OpenSceneCommandArguments : IUtilityCommandArguments
    {
        public string Path;

        public OpenSceneCommandArguments(string path)
        {
            Path = path;
        }
    }
}