﻿using Test_Assignment.Interfaces;

namespace Test_Assignment.Structs.CommandArguments.Shape
{
    public struct RemoveShapesCommandArguments : IShapeCommandArguments
    {
        public int[] ShapesToRemove;

        public RemoveShapesCommandArguments(params int[] shapesToRemove)
        {
            ShapesToRemove = shapesToRemove;
        }
    }
}