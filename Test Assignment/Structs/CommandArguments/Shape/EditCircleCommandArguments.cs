﻿using Test_Assignment.Interfaces;
using Test_Assignment.Structs.Utility;

namespace Test_Assignment.Structs.CommandArguments.Shape
{
    public struct EditCircleCommandArguments : IShapeCommandArguments
    {
        public int ShapeIndex;
        public Vector2 Position;
        public float Radius;
        public Color Color;
        public OutlineProperties OutlineProperties;

        public EditCircleCommandArguments(int shapeIndex, Vector2 position, float radius, Color color, OutlineProperties outlineProperties)
        {
            ShapeIndex = shapeIndex;
            Position = position;
            Radius = radius;
            Color = color;
            OutlineProperties = outlineProperties;
        }
    }
}