﻿using Test_Assignment.Interfaces;
using Test_Assignment.Structs.Utility;

namespace Test_Assignment.Structs.CommandArguments.Shape
{
    public struct AddLineCommandArguments : IShapeCommandArguments
    {
        public Vector2 StartPosition;
        public Vector2 EndPosition;
        public Color Color;
        public OutlineProperties OutlineProperties;

        public AddLineCommandArguments(Vector2 startPosition, Vector2 endPosition, Color color, OutlineProperties outlineProperties)
        {
            StartPosition = startPosition;
            EndPosition = endPosition;
            Color = color;
            OutlineProperties = outlineProperties;
        }
    }
}