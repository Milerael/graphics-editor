﻿using Test_Assignment.Interfaces;
using Test_Assignment.Structs.Utility;

namespace Test_Assignment.Structs.CommandArguments.Shape
{
    public struct EditRectangleCommandArguments : IShapeCommandArguments
    {
        public int ShapeIndex;
        public Vector2 Position;
        public Vector2 Size;
        public Color Color;
        public OutlineProperties OutlineProperties;

        public EditRectangleCommandArguments(int shapeIndex, Vector2 position, Vector2 size, Color color, OutlineProperties outlineProperties)
        {
            ShapeIndex = shapeIndex;
            Position = position;
            Size = size;
            Color = color;
            OutlineProperties = outlineProperties;
        }
    }
}