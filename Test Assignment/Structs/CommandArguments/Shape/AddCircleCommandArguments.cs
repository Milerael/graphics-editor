﻿using Test_Assignment.Interfaces;
using Test_Assignment.Structs.Utility;

namespace Test_Assignment.Structs.CommandArguments.Shape
{
    public struct AddCircleCommandArguments : IShapeCommandArguments
    {
        public Vector2 Position;
        public float Radius;
        public Color Color;
        public OutlineProperties OutlineProperties;

        public AddCircleCommandArguments(Vector2 position, float radius, Color color, OutlineProperties outlineProperties)
        {
            Position = position;
            Radius = radius;
            Color = color;
            OutlineProperties = outlineProperties;
        }
    }
}