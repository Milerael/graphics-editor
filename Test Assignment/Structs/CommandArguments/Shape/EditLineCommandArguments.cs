﻿using Test_Assignment.Interfaces;
using Test_Assignment.Structs.Utility;

namespace Test_Assignment.Structs.CommandArguments.Shape
{
    public struct EditLineCommandArguments : IShapeCommandArguments
    {
        public int ShapeIndex;
        public Vector2 StartPosition;
        public Vector2 EndPosition;
        public Color Color;
        public OutlineProperties OutlineProperties;

        public EditLineCommandArguments(int shapeIndex, Vector2 startPosition, Vector2 endPosition, Color color, OutlineProperties outlineProperties)
        {
            ShapeIndex = shapeIndex;
            StartPosition = startPosition;
            EndPosition = endPosition;
            Color = color;
            OutlineProperties = outlineProperties;
        }
    }
}