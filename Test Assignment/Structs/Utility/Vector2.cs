﻿using System;

namespace Test_Assignment.Structs.Utility
{
    [Serializable]
    public struct Vector2
    {
        public float X;
        public float Y;

        public Vector2(float x, float y)
        {
            X = x;
            Y = y;
        }

        public string Formatted => $"X: {X}, Y: {Y}";
    }
}