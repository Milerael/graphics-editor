﻿using System;

namespace Test_Assignment.Structs.Utility
{
    [Serializable]
    public struct Color
    {
        public float R, G, B;

        public Color(float r, float g, float b)
        {
            R = r;
            G = g;
            B = b;
        }
        
        public string Formatted => $"R: {R}, G: {G}, B: {B}";
    }
}