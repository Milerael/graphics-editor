﻿using System;

namespace Test_Assignment.Structs.Utility
{
    [Serializable]
    public struct OutlineProperties
    {
        public float OutlineWidth;

        public OutlineProperties(float outlineWidth)
        {
            OutlineWidth = outlineWidth;
        }
        
        public string Formatted => $"OutlineWidth: {OutlineWidth}";
    }
}